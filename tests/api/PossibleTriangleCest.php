<?php

class PossibleTriangleCest
{

//  1. Позитивный тест

    public function isPossibleTriangle(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 5,
            'b' => 6,
            'c' => 9
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"isPossible":true}');
    }

// 2. Позитивный тест, где хотя бы одно из значений делится на 10 без остатка

    public function isPossibleTriangleWithTen(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 9,
            'b' => 7,
            'c' => 10
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"isPossible":true}');
    }

    //3. Сумма длин двух сторон треугольника может быть только больше длины третьей стороны,
    //   т.е. a<b+c, b<a+c, c<a+b

    public function isPossibleTriangleWhereThirdSideLonger(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 23,
            'b' => 25,
            'c' => 76
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"isPossible":false}');
    }

    // 4. Тест, где сумма длин двух сторон треугольника равна длине третьей стороны

    public function isPossibleTriangleWhereSumSidesEqualThird(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 4,
            'b' => 4,
            'c' => 8
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"isPossible":false}');
    }

    // 5. Тест равностороннего треугольника

    public function isPossibleTriangleEquilateral(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 14,
            'b' => 14,
            'c' => 14
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"isPossible":true}');
    }

    // 6. Тестирование граничных значений (два параметра)

    public function isPossibleTriangleWithTwoParams(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 23,
            'b' => 25
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 7. Тестирование граничных значений (четыре параметра)

    public function isPossibleTriangleWithFourParams(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 23,
            'b' => 25,
            'c' => 22,
            'd' => 29
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 8. Тест, в котором не отправляются параметры

    public function isPossibleTriangleWithoutParams(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 9. Тест, в котором отправляются параметры без значений

    public function isPossibleTriangleWithEmptyParams(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => '',
            'b' => '',
            'c' => ''
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 10. Тест, в котором передается десятичная дробь вместо натурального числа

    public function isPossibleTriangleWithFractionalNumber(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 23.3,
            'b' => 25,
            'c' => 20
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 11. Тест, в котором передается отрицательное число вместо натурального

    public function isPossibleTriangleWithNegativeNumber(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 23,
            'b' => -25,
            'c' => 20
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 12. Тест, в котором передаются нули в качестве значений сторон треугольника

    public function isPossibleTriangleWithZeroSides(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 0,
            'b' => 0,
            'c' => 0
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }

    // 13. Тест, в котором передается символ, не являющийся числом

    public function isPossibleTriangleWithLetter(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/triangle/possible', [
            'a' => 'A',
            'b' => 25,
            'c' => 20
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST); // 400
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"message":{"error":"Not valid date"}}');
    }



}
